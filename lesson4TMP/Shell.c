#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "LineParser.h"
void DirName();
void execute(cmdLine *pCmdLine);
int main()
{
	cmdLine* cmd;
	FILE* commandFile;//cmd.txt, the commands will read from here
	commandFile = fopen("cmd.txt", "r+");
	char command[2049];//2048 for the command and 1 for null
	while(1)
	{
		DirName();
		printf(" ");
		fgets(command, 2048, commandFile);
		cmd = _parseCmdLines(command);
		if(strcmp(command, "quite"))
		{
			exit(0);
		}
		else
		{
			execute(cmd);
			freeCmdLine(cmd);
		}
	}
}
/*the functions in LineParser.c and LineParser.h is magshimim code, the functions below are mine*/


/*DirName - print the current directory name */
void DirName()
{
	char dir[1024];
   if (getcwd(dir, sizeof(dir)) != NULL)
   {
       fprintf(stdout, "%s\n", dir);
   }
}
/*execute - get parsed struct and do the command in syscall  execv*/
void execute(cmdLine *pCmdLine)
{
	if((execvp(pCmdLine->arguments[0], pCmdLine->arguments)))
	{
		execvp(pCmdLine->arguments[0], pCmdLine->arguments);	
	}
	else
	{
		perror("Error: ");
	}
}