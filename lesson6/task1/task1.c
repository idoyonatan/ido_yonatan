#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
int main()
{
	int child;
	child = fork();
	if(child == 0)
	{
		while(1)
		{
			printf("im still alive!\n");
		}
		exit(1);
	}
	else
	{
		sleep(1);
		printf("Dispatching\n");
		kill(child, SIGKILL);
		printf("Dispatched\n");	
	}
}

