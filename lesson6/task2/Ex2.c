#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
void quit(int);
#define LOOP 2000000000
int main()
{
	void (*Quit)(int);
	Quit = &quit;
	int i;
	int count = 1000000;
	for(i = 1; i <= LOOP; i++)
	{
		signal(SIGINT, Quit);
		signal(SIGQUIT, Quit);
		signal(SIGTERM, Quit);
		if(i == count)
		{
			printf("im still alive %d\n", count);
			count += 1000000;
		}
	}
	exit(EXIT_SUCCESS);
}
void quit(int sigNum)
{
	char choice[1025];
	int i, flag = 0;
	printf("do you want to exit? ");
	for(i=0; 1 < 1025 || flag == 0; i++)
	{
		scanf("%c", &choice[i]);
		if(*(choice) == 'y' || *(choice) == 'Y')
		{
			exit(1);
		}
		else if(*(choice + i) = 13)
		{
			flag = 1;
		}
	}
}