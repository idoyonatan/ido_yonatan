#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "LineParser.h"
void DirName();
void execute(cmdLine *pCmdLine);
int main()
{
	cmdLine* cmd;
	FILE* commandFile;//cmd.txt, the commands will read from here
	commandFile = fopen("cmd", "r");
	if(commandFile == NULL)
	{
		printf("errore opening file");
	}
	char command[2049];//2048 for the command and 1 for null
	while(1)
	{
		int cpid = fork();
		if(cpid == 0)
		{
			DirName();
			printf(" ");
			if((fgets(command, 2049, commandFile) != NULL))
			{
				puts(command);
				printf("%s", command);
			}
			cmd = parseCmdLines(command);
			execute(cmd);
			//freeCmdLines(cmd);
		}
		if(cmd->blocking == 0)
		{
			waitpid(-1);
		}
		
	}
}
/*the functions in LineParser.c and LineParser.h is magshimim code, the functions below are mine*/



/*void myEcho(char command[])
{
	int i, lenght;
	lenght = strlen(command);
	for(i = 7; i != lenght; i++)//i=7, 6 for myEcho and 1 for space
	{
		putch(command[i]);
	}
}*/
/*DirName - print the current directory name*/
void DirName()
{
	char dir[1024];
   if (getcwd(dir, sizeof(dir)) != NULL)
   {
       fprintf(stdout, "%s\n", dir);
   }
}
/*execute - get parsed struct and do the command in syscall  execv*/
void execute(cmdLine *pCmdLine)
{
	if(execvp(pCmdLine->arguments[0], pCmdLine->arguments))
	{
		execvp(pCmdLine->arguments[0], pCmdLine->arguments);	
	}	
	else
	{
		perror("error");
	}
}
