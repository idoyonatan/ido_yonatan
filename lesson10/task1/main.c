#include <elf.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("you need to put file name\n");
		return 1;
	}
	int index = 0;
	char* name;
	struct stat *buf;
	int file = open(argv[1], O_RDONLY);
	stat(argv[1], buf);
	char *p = mmap(0, buf->st_size, PROT_READ, MAP_PRIVATE, file, 0);
	Elf32_Ehdr *header = (Elf32_Ehdr*)p;
	Elf32_Shdr *section = (Elf32_Shdr *)(p + header->e_shoff);
	for(index = 0; index < header->e_shnum; index++)
	{
		printf("[%d] ", index);
		//printf("%s ", (char*)section[index].sh_name);
		printf("%d ", section[index].sh_addr);
		printf("%d ", section[index].sh_offset);
		printf("%d\n", section[index].sh_size);
	}
}