#include "ceaser.h"
char shift_letter(char letter, int offset)
{
	char ret;//value to return
	if(letter+offset > 'z')
	{
		ret = letter+offset-26;
	}
	else 
	{
		ret = letter+offset;
	}
	return ret;
}

char * shift_string(char * input, int offset)
{
	int length = sizeof input;
	int i;
	char encrypted[length];
	char a;
	for (i = 0; i < length; ++i)
	{

		a = shift_letter(input[i],offset);
		encrypted[i] = a;
	}
	encrypted[i-1] = 00;
	return encrypted;
}