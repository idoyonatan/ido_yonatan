#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "LineParser.h"
void DirName();
void execute(cmdLine *pCmdLine);
int main()
{
	cmdLine* cmd;
	int cpid;
	FILE* commandFile;//cmd.txt, the commands will read from here
	commandFile = fopen("cmd", "r");
	if(commandFile == NULL)
	{
		printf("errore opening file");
	}
	char command[2049];//2048 for the command and 1 for null
	while(1)
	{
		if(cpid == 0)
		{
			DirName();
			printf(" ");
			if((fgets(command, 2049, commandFile) != NULL))
			{
				puts(command);
				cmd = parseCmdLines(command);
				execute(cmd);
			}
			else
			{
				waitpid(cpid, 0, 0);
				exit(0);	
			}
		}		
	}
	freeCmdLines(cmd);
}
/*the functions in LineParser.c and LineParser.h is magshimim code, the functions below are mine*/



/*void myEcho(char command[])
{
	int i, lenght;
	lenght = strlen(command);
	for(i = 7; i != lenght; i++)//i=7, 6 for myEcho and 1 for space
	{
		putch(command[i]);
	}
}*/
/*DirName - print the current directory name*/
void DirName()
{
	char dir[1024];
   if (getcwd(dir, sizeof(dir)) != NULL)
   {
       fprintf(stdout, "%s", dir);
   }
}
/*execute - get parsed struct and do the command in syscall  execv*/
void execute(cmdLine *pCmdLine)
{
	int pid = fork();
	if(pid == 0)
	{
		execvp(pCmdLine->arguments[0], pCmdLine->arguments);
		perror("error\n");
		exit(0);
	}	
	else
	{
		waitpid(pid, 0, 0);
	}

}
