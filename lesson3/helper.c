#include "helper.h"
/* return the length of a string */
int slen(const char * str)
{
	int i = -1;
	while (str[++i]);

	return i;
}

#define BUFFER_SIZE 13

char buffer[BUFFER_SIZE];

/* returns the string representation of an integer */
char * __itoa(int num)
{
	char* p = buffer+BUFFER_SIZE-1;
	int neg = num<0;
	
	if(neg)
		num = -num;
	
	*p='\0';
	do 
		*(--p) = '0' + num%10; 
	while(num/=10);
	
	if(neg) 
		*(--p) = '-';
	
	return p;
}
void myExit()
{
	exit(1);
}
void printMenue()
{
	write(1, "put 1 to exit, 2 to print the user id, 3 to print the current dir\n", 69);
}
void GetID()
{
	int *id;
	id = getuid();
    write(1, id, 30);
}
void getDirName()
{
	char *cwd =  get_current_dir_name(); 
   	write(1, cwd, 43);
}
