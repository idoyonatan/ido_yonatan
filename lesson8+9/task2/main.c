#include <stdio.h>
#include <string.h>
#include <unistd.h>
void* malloc(size_t size);
void free(void * p_block);
struct metadata_block 
{
	size_t size;
	struct metadata_block *next;
	int free;
};
typedef struct metadata_block * p_block;
p_block firstLink = NULL;
int main()
{
	char * str = malloc(10);
	void * ptr = (void *) str;
	void * ptr2;
	
	strcpy(str,"TEST!\0");

	free(str);
	printf("free: OK!\n");

	str = malloc(15);
	if ((void*) str != ptr)
	{
		printf("malloc() should've used the freed memory! old: %p new: %p\n",ptr,str);
		return 1;
	}
	printf("sdfsdf");
}

void free(void * ptr)
{
	p_block tmp = ptr-sizeof(struct metadata_block);
	int addr = tmp->size+sizeof(struct metadata_block);
	addr *= -1;
	if(tmp->next)
	{
		tmp->free = 1;//1-free
	}
	else
	{
		sbrk(addr);
	}
}
void* malloc(size_t size)
{
	p_block tmp;
	if(firstLink == NULL)
	{
		printf("2\n");
		firstLink = sbrk(size+sizeof(struct metadata_block));
		firstLink->next = NULL;
		firstLink->size = size;
		firstLink->free = 0;
		return (void*)firstLink+sizeof(struct  metadata_block);
	}
	else
	{
		tmp = firstLink;
		while(tmp->next)
		{
			if(size >= tmp->size && tmp->free == 1)
			{
				tmp->free = 0; 
				tmp->size = size;
				return tmp + sizeof(tmp);
			}
			else
			{
				tmp = tmp->next;
			}
		}
	}
		p_block newBlock;
		newBlock = sbrk(size+sizeof(struct metadata_block));
		newBlock->size = size;
		newBlock->next = NULL;
		newBlock->free = 0;
		tmp->next = newBlock;
		return (void*)newBlock+sizeof(struct metadata_block); 
}
void * realloc(void * ptr,size_t size) 
{
	p_block tmp = ptr-sizeof(p_block);
	p_block new = malloc(size);
	new = tmp;
	free(ptr);
	return (void*)new+sizeof(struct metadata_block);
}