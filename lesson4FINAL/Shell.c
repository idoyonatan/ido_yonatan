#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "LineParser.h"
#define CHILD 0
#define COMMAND_LENGHT 2049
#define DIRECTORY_LENGHT 1024
void DirName();
void execute(cmdLine *pCmdLine);
int main()
{
	cmdLine* cmd;
	int cpid;
	FILE* commandFile;//cmd.txt, the commands will read from here
	commandFile = fopen("cmd", "r");
	if(commandFile == NULL)
	{
		printf("errore opening file");
	}
	char command[COMMAND_LENGHT];//2048 for the command and 1 for null
	while(1)
	{
		if(cpid == CHILD)
		{
			DirName();
			printf(" ");
			if((fgets(command, COMMAND_LENGHT, commandFile) != NULL))
			{
				puts(command);
				cmd = parseCmdLines(command);
				execute(cmd);
			}
			else
			{
				waitpid(cpid, 0, 0);
				exit(0);	
			}
		}		
	}
	freeCmdLines(cmd);
}
/*the functions in LineParser.c and LineParser.h is magshimim code, the functions below are mine*/



/*DirName - print the current directory name*/
void DirName()
{
	char dir[DIRECTORY_LENGHT];
   if (getcwd(dir, sizeof(dir)) != NULL)
   {
       fprintf(stdout, "%s", dir);
   }
}
/*execute - get parsed struct and do the command in syscall  execv*/
void execute(cmdLine *pCmdLine)
{
	int pid = fork();
	if(pid == CHILD)
	{
		execvp(pCmdLine->arguments[0], pCmdLine->arguments);
		perror("error\n");
		exit(0);
	}	
	else
	{
		waitpid(pid, 0, 0);
	}

}
