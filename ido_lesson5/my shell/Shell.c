#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "LineParser.h"
#define CHILD 0
#define COMMAND_LENGHT 2049
#define DIRECTORY_LENGHT 1024
void Pipe(cmdLine *pCmdLine);
void DirName();
void execute(cmdLine *pCmdLine);
void redirection(cmdLine *pCmdLine);
int main()
{
	cmdLine* cmd;
	int cpid;
	int pipefd[2];
	FILE* commandFile;//cmd.txt, the commands will read from here
	commandFile = fopen("cmd", "r");
	if(commandFile == NULL)
	{
		printf("errore opening file");
	}
	char command[COMMAND_LENGHT];//2048 for the command and 1 for null
	while(1)
	{
		if(cpid == CHILD)
		{
			DirName();
			printf(" ");
			if((fgets(command, COMMAND_LENGHT, commandFile) != NULL))
			{
				puts(command);
				cmd = parseCmdLines(command);
				execute(cmd);
			}
			else
			{
				waitpid(cpid, 0, 0);
				exit(0);	
			}
		}		
	}
	freeCmdLines(cmd);
}
/*the functions in LineParser.c and LineParser.h is magshimim code, the functions below are mine*/



/*DirName - print the current directory name*/
void DirName()
{
	char dir[DIRECTORY_LENGHT];
   if (getcwd(dir, sizeof(dir)) != NULL)
   {
       fprintf(stdout, "%s", dir);
   }
}
/*execute - get parsed struct and do the command in syscall  execv*/
void execute(cmdLine *pCmdLine)
{
	int pipefd[2];
	int pid, child;
	cmdLine *nextCommand;
	nextCommand = pCmdLine->next;
	if(pCmdLine->next)
	{
		pipe(pipefd);
		pid = fork();
		if(pid == 0)//make the command befor the pipe
		{
			close(1);
			dup(pipefd[1]);
			execvp(pCmdLine->arguments[0], pCmdLine->arguments);
		}
		close(pipefd[1]);
		child = fork();
		if(child == 0)//make the command after the pipe
		{
			
			close(0);
			dup(pipefd[0]);
			execvp(nextCommand->arguments[0], nextCommand->arguments);
		}
		close(pipefd[0]);
	}
	else
	{
		pid = fork();
		if(pid == CHILD)
		{
			redirection(pCmdLine);
			execvp(pCmdLine->arguments[0], pCmdLine->arguments);
			perror("error\n");
			exit(0);
		}	
		else
		{
			waitpid(pid, 0, 0);
		}
	}
}
void redirection(cmdLine *pCmdLine)
{
	int input;
	int output;
	if(pCmdLine->inputRedirect)
	{
		input = open(pCmdLine->inputRedirect, O_RDONLY);
		close(0);
		dup(input);
		if(pCmdLine->outputRedirect)
		{
			output = open(pCmdLine->outputRedirect, O_WRONLY);
			close(1);
			dup(output);
		}
	}

}