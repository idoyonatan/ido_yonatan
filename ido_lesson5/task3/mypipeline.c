#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
int main()
{
	int child1, child2;
	char * argTail[4];
	argTail[0] = "tail";
	argTail[1] = "-n";
	argTail[2] = "2";
	argTail[3] = NULL;
	char * argLS[3];
	argLS[0] = "/bin/ls";
	argLS[1] = "-l";
	argLS[2] = NULL;
	int pipefd[2];
	pipe(pipefd);
	child1 = fork();
	if(child1 == 0)
	{
		close(1);
		dup(pipefd[1]);
		execvp(argLS[0], argLS);
	}
	close(pipefd[1]);
	child2 = fork();
	if (child2 == 0)
	{
		close(0);
		dup(pipefd[0]);
		execvp(argTail[0], argTail);
	}
	close(pipefd[0]);
}