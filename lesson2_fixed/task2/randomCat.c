#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#define MAX_LINES 6//for example
void printLine(FILE* file, int line);
int main()
{	
	srand(time(NULL));
	int maxLines;
	int line; 
	FILE * file;
	file = fopen("file.txt", "r");
    line = rand() % MAX_LINES + 1;
    printLine(file, line);
	return 0;
}
void printLine(FILE* file, int line)
{
	int nl = 0, c;//number of lines
	c = getc(file);
    while(c != EOF)
    {
        if(c == '\n')
        {
        	nl++;
            if(nl == line)
            {
                c = getc(file);
                while(c != '\n')
                {
                    printf("%c", c);
                    c = getc(file);
                }
            }
    	}
        c = getc(file);
    }
    printf("\n");
}
