#include "ceaser.h"
#include "string.h"
char shift_letter(char letter, int offset)
{
	char ret;//value to return
	if(letter+offset > 'z')
	{
		ret = letter+offset-26;
	}
	else 
	{
		ret = letter+offset;
	}
	return ret;
}

char * shift_string(char * input, int offset)
{
	int length = strlen(input);
	int i;
	char *encrypted = (char*)malloc(sizeof(length));
	char a;
	for (i = 0; i <= length; ++i)
	{
		a = *(input+i);
		a = shift_letter(a ,offset);
		encrypted[i] = a;
	}
	encrypted[i-1] = 00;
	return encrypted;
}
